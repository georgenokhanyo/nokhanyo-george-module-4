import 'package:flutter/material.dart';
import 'package:animated_splash/animated_splash.dart';

void main() {
Function duringSplash = () {
	print('Background');
	int a = 123 + 23;
	print(a);

	if (a > 100)
	return 1;
	else
	return 2;
};

Map<int, Widget> op = {1: Home(), 2: HomeState()};

runApp(MaterialApp(
	home: AnimatedSplash(
	imagePath: 'https://images.app.goo.gl/nkfRR1v9hF2vcheq9',
	home: Home(),
	customFunction: duringSplash,
	duration: 3000,
	type: AnimatedSplashType.BackgroundProcess,
	outputAndHome: op,
	),
));
}

class Home extends StatefulWidget {
@override
_HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
@override
Widget build(BuildContext context) {
	return Scaffold(
		appBar: AppBar(
		title: Text('Welcome to Google'),
		backgroundColor: Colors.pink,
		),
		body: Center(
			child: Text('My Home Page',
				style: TextStyle(color: Colors.black,
					fontSize: 17.0))));
}
}
