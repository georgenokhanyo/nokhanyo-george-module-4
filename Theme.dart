title: 'Welcome to Google',//change
 theme: ThemeData(
 brightness: Brightness.white,
 primaryColor: Colors.white[800],
 
 fontFamily: 'New Times Roman',//change
 textTheme: TextTheme(
  headline1: TextStyle(fontSize: 60, fontWeight: FontWeight.bold),
 ),
 ),
 home: HomeState(),
);
